package com.example.laptop.project;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tv_01,tv_02;
    private ImageButton ib_start;
    MediaPlayer mediaPlayer;
    Notification.Builder notification;
    final int id = 11;
    final int id_progress = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayer = MediaPlayer.create(getApplicationContext(),R.raw.gaikoz);

        tv_01 = (TextView)findViewById(R.id.tv_01);
        tv_02 = (TextView)findViewById(R.id.tv_02);

        ib_start = (ImageButton)findViewById(R.id.ib_start);

        ib_start.setOnClickListener(this);


    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {

            case R.id.ib_start:

                if ( mediaPlayer.isPlaying())
                {
                    mediaPlayer.stop();
                }

                tv_02.setText("Please wait for about 3 minutes ");

                notification = new Notification.Builder(MainActivity.this);
                notification.setWhen(System.currentTimeMillis());
                notification.setSmallIcon(R.drawable.ic_launcher);
                notification.setContentTitle("Cooking");
                notification.setTicker("maill");
                notification.setContentText("Wait for about 3 minutes ");

                final NotificationManager nm=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 1; i<=20;i++)
                        {
                            try{
                                Thread.sleep(9000);
                            }catch (InterruptedException e){
                                e.printStackTrace();
                            }
                            notification.setProgress(20,i,false);
                            nm.notify(id_progress, notification.build());
                        }
                        mediaPlayer.start();
                        notification.setContentTitle("Cooked");
                        notification.setContentText("Enjoy your meal!");
                        notification.setProgress(0, 0, false);
                        nm.notify(id_progress, notification.build());


                    }
                }).start();

                new CountDownTimer(180000, 1000)
                {
                    public void onTick(long millisUntilFinished)
                    {
                        int second = (int) (millisUntilFinished / 1000);
                        tv_01.setText("Wait: " + second +"  second");

                    }

                    public void onFinish()
                    {
                        tv_02.setText("");
                        tv_01.setText("Cooked");
                        ib_start.setEnabled(true);
                    }
                }.start();
                ib_start.setEnabled(false);
                break;

        }

    }


}
